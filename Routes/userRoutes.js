const express = require("express")
const router = express.Router()

const UserController = require("../controllers/userControllers")

const auth = require("../auth")

//Register User
router.post("/register", (req, res) => {
	UserController.registerUser(req.body).then(result => res.send(result))
})

//Login User for getiing the Token
router.post("/userLogin", (req, res) => {
	UserController.userLogin(req.body).then(result => res.send(result))
})

//Check if emails are existing
router.post("/checkEmail", (req, res) => {
	UserController.checkEmailExists(req.body).then(result => res.send(result))
})

//Orders checkout
router.post("/checkOut", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId
	}
	UserController.checkOut(data).then(result => res.send(result))
})

//Retrieve specific users orders
router.get("/usersOrder", auth.verify, (req, res) => {
	let userdata = auth.decode(req.headers.authorization).id
	console.log(auth.decode(req.headers.authorization))	
	console.log(userdata)
	UserController.getOrders(userdata).then(result => res.send(result))

})

//Retrieve all users who have orders 
router.get("/allOrders", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin
	if (isAdmin) {
		UserController.getAllOrders().then(result => res.send(result))
	}else{
		res.send(false)
	}
})

// router.put("/paymentVerify/:id", auth.verify, (req, res) => {
// 	let isAdmin = auth.decode(req.headers.authorization).isAdmin
// 	if (isAdmin) {
// 		UserController.paymentVerify(req.params.id).then(result => res.send(result))
// 	}else{
// 		res.send(false)
// 	}
// })

module.exports = router;