const express = require("express")
const router = express.Router()
const ProductController = require("../controllers/productControllers")
const auth = require("../auth");

//Admin adding a product
router.post("/addProducts", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
		ProductController.addProducts(data).then(result => res.send(result))
	}else{
		res.send({ auth: "Hey! You're not an admin"})
	}
})

//Getting all active products
router.get("/all", (req, res) => {
	ProductController.getAllActive().then(result => res.send(result))
})

//Getting single product using params
router.get("/:productId", (req, res) => {
	ProductController.getSingleProduct(req.params.productId).then(result => res.send(result))
})

//Updating a products
router.put("/:productId", auth.verify, (req, res) => {
	const productData = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(productData.isAdmin){
		ProductController.updateProducts(req.params.productId, req.body).then(result => res.send(result))
	}else{
		res.send(false)
	}
})


//Archiving a products
router.put("/:productId/archive", auth.verify, (req, res) => {
	const productsData = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(productsData.isAdmin){
		ProductController.archiveProduct(req.params.productId).then(result => res.send(result))
	}else{
		res.send(false)
	}
})

module.exports = router;