const express = require("express");
const mongoose = require("mongoose");
require('dotenv').config();
const cors = require("cors")

const userRoutes = require("./Routes/userRoutes")
const productRoutes = require("./Routes/productRoutes")

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}))

app.use("/capstone2", userRoutes)
app.use("/capstone2/products", productRoutes)

mongoose.connect("mongodb+srv://deanrodel:deanrodel@clusterb164.wo1ny.mongodb.net/Capstone2?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas"))


app.listen(process.env.PORT, () => {
	console.log(`API is now on port ${process.env.PORT}`)
})