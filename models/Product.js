const mongoose = require("mongoose")

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "product name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "price is required"]
	},
	stocks: {
		type: Number,
		required: [true, "stocks is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	usersOrder: [{
		userId: {
			type: String,
			required: [true, "User ID is required"]
		},
		addedOn: {
			type: Date,
			default: new Date()
		}
	}]
})


module.exports = mongoose.model("Product", productSchema)