const Product = require("../models/Product")

module.exports.addProducts = (reqBody) => {
console.log(reqBody)

	let newProduct = new Product ({
		name: reqBody.product.name,
		description: reqBody.product.description,
		price: reqBody.product.price,
		stocks: reqBody.product.stocks
	})

	return newProduct.save().then((product, error) => {
		if (error) {
			return false
		}else{
			return true
		}
	})
}

module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {
		return result
	})
}

module.exports.getSingleProduct = (reqParams) => {
	return Product.findById(reqParams).then(result => {
		return result
	})
}

module.exports.updateProducts = (productId, reqBody) => {
	let updateProducts = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks
	}

	return Product.findByIdAndUpdate(productId, updateProducts).then((product, error) => {
		if(error){
			return false
		}else{
			return true
		}
	})
}

module.exports.archiveProduct = (productId)=>{
	let archiveProductData = {
		price: 1299
	}
	return Product.findByIdAndUpdate(productId, archiveProductData).then((product, error) => {
		if(error){
			return error
		}else{
			return true
		}
	})
}

