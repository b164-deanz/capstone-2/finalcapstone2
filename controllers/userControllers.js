const User = require("../models/User")
const Product = require("../models/Product")

const bcrypt = require("bcrypt")

const auth = require("../auth")

module.exports.registerUser = (reqBody) => {
	let registerUsers = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 8)
	})
	return registerUsers.save().then((register, error) => {
		if (error) {
			return false
		}else{
			return true
		}
	})
}

module.exports.userLogin = (reqBody)=>{
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){
				return { YourToken: auth.createAccessToken(result.toObject())}
			}else{
				return false
			}
		}
	})
}

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {

		//if match is found
		if(result.length > 0){
			return result
		}else{
			//No duplicate email found
			//The user is not yet registered in the database
			return false
		}
	})
}

module.exports.checkOut = async(data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.orders.push({productId: data.productId})
		return user.save().then((user, error) => {
			if(error){
				return false
			}else{
				return true
			}
		})
	})

	let isProductUpdated = await Product.findById(data.productId).then(product => {
		product.usersOrder.push({userId: data.userId})
		return product.save().then((product, error) => {
			if(error){
				return false
			}else{
				return true
			}
		})
	})

	if(isUserUpdated && isProductUpdated){
		return true
	}else{
		return false
	}
}

module.exports.getOrders = (data) => {
	return User.findOne({_id: data}, {orders: 1, email: 1}).then(result => {
		if(result == null){
			return false
		}else{
			return result
		}
	})
}

module.exports.getAllOrders = (orders) => {
	return User.find({}).then(result => {
		let getAllOrders = []

		for(let i=0; i<result.length; i++){
			if(result[i].orders != 0){
				getAllOrders.push(result[i].firstName, result[i].lastName,result[i].orders)
			}else{}
		}
		return getAllOrders
	})	
}

// module.exports.paymentVerify = (userId) => {
// 	let users = {
// 		paymentStatus: true
// 	}
// 	return User.findByIdAndUpdate(userId, users).then((user, error) => {
// 		if(error){
// 			return errorr
// 		}
// 		else{			
// 			return user
// 		}
// 	})
// }